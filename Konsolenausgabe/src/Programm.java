
public class Programm {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Guten Morgen");
		System.out.println("Beispielsätze sind toll.\nSuper tolle Beispielsätze.");
		System.out.println("Er sagte: \"Guten Tag!\"");
		System.out.println("// Bei println wird nach der Ausgabe ein zusätzlicher Zeilenumbruch eingefügt");
		int alter = 56; 
		String name = "Willi"; 
		System.out.println("Ich heiße " + name + " und bin " + alter + " Jahre alt."); 
					
		
		System.out.println("\n      *\n     ***\n    *****\n   *******\n  *********\n ***********\n*************\n     ***\n     ***");
		
		
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		
		
		
		System.out.println("             * *");
		System.out.println("           *     *");
		System.out.println("           *     *");
		System.out.println("             * *");
		
		
		
		String s1 = "Celsius"; 
		String s2 = "Fahrenheit";
		System.out.printf( "%-12s|", s2 );
		System.out.printf( "%10s\n", s1 );
		System.out.printf("-----------------------");
		
	}

}
